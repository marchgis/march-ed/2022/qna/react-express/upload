# Upload File dari React ke Express JS Menggunakan Multer

## Express Backend
1. Masuk ke folder express-backend
2. Jalankan perintah
  ```sh
  npm install
  ```

## React Frontend 
1. Masuk ke folder react-frontend
2. Jika menggunakan Yarn, jalankan perintah
  ```sh
  yarn
  ```
  Jika menggunakan NPM, jalankan perintah
  ```sh
  npm install
  ```
