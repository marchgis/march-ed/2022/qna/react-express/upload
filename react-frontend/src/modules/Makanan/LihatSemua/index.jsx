import "./style.css"

const MakananLihatSemua = () => {
  return (
    <div
      className="MakananLihatSemua"
    >
      <h2>Lihat Semua Makanan</h2>
    </div>
  )
}

export default MakananLihatSemua
