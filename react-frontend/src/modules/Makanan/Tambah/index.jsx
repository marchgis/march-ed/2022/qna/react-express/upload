import { useState } from "react"
import "./style.css"

const MakananTambah = () => {

  /**
    * State: input
    * Representasi dari input makanan yang berisi
    * - nama
    * - harga
    * - foto
    */
  const [input, setInput] = useState({
    nama: "",
    harga: 5000,
    foto: null,
  })

  /**
    * Function: tambah
    * Menambahkan makanan hasil input ke server
    * menggunakan HTTP method POST ke RESTful API http://localhost:3100/makanan
    */
  const tambah = () => {

    // URL dari RESTful API untuk menambahkan makanan
    const url = "http://localhost:3100/makanan"

    // new FormData() merupakan objek 
    // yang digunakan untuk mewadahi body
    // dengan content-type form-data (beda dengan JSON: application/json)
    // Biasa digunakan untuk mengirim data bersama dengan file
    const dataUntukDikirim = new FormData()

    // Mengisi FormData dataUntukDikirim
    // dengan input makanan
    // yang sudah diketik
    dataUntukDikirim.append("nama", input.nama)
    dataUntukDikirim.append("harga", input.harga)
    dataUntukDikirim.append("foto", input.foto)

    // Melakukan REQUEST POST
    // menggunakan fetch()
    // untuk mengirimkan input makanan ke server
    // melalui URL RESTful API
    fetch(url, {
      method: "post",
      body: dataUntukDikirim,
    })
  }

  return (
    <div
      className="MakananTambah"
    >
      <h2>Tambah Makanan</h2>

      {input.foto &&
        <img
          src={(() => {

            /**
              * fungsi createObjectURL dari object URL
              * berfungsi untuk membuat URL dari sebuah objek 
              */

            return URL.createObjectURL(input.foto)
          })()}
          height={200}
        />
      }
      <div
        className="MakananTambah_form"
      >
        <div
          className="MakananTambah_form-item"
        >
          <small>Nama Makanan</small>
          <input
            placeholder="misal: Seblak Enterprise 2024"
            value={input.nama}
            onChange={event => {
              // Mengubah atribut "nama" pada State "input"
              // event.target.value adalah cara untuk mengambil nilai yang diketik
              // pada input ini
              setInput(
                {...input, nama: event.target.value}
              )
            }}
          /> 
        </div>
        <div
          className="MakananTambah_form-item"
        >
          <small>Harga</small>
          <input
            placeholder="Harga"
            type="number"
            step={500}
            value={input.harga}
            onChange={event => {
              // Mengubah atribut "harga" pada State "input"
              // event.target.value adalah cara untuk mengambil nilai yang diketik
              // pada input ini
              setInput(
                {...input, harga: event.target.value}
              )
            }}
          /> 
        </div>
        <div
          className="MakananTambah_form-item"
        >
          <small>Foto</small>
          <input
            accept="image/*"
            onChange={event => {

              // event.target.files[0] adalah cara untuk mengambil file pertama yang dipilih 
              // pada input ini
              const foto = event.target.files[0]

              // Mengubah atribut "foto" pada State "input"
              // menggunakan file foto yang telah didapatkan 
              setInput(
                {...input, foto: foto}
              )
            }}
            type="file"
          />
        </div>
        <button
          onClick={tambah}
        >
          Tambah
        </button>
      </div>
    </div>
  )
}

export default MakananTambah
