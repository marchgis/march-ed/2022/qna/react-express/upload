import MakananTambah from "./modules/Makanan/Tambah"
import MakananLihatSemua from "./modules/Makanan/LihatSemua"
import "./App.css"

const App = () => {
  return (
    <div
      className="App"
    >
      <MakananTambah />
      <MakananLihatSemua />
    </div>
  )
}

export default App
