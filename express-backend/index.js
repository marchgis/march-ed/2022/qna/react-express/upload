// Untuk membuat web page ataupun web service
const express = require('express')

// Untuk menangani Cross Origin Resource Sharing
// Menentukan siapa saja yang boleh mengakses Resource di web ini
const cors = require('cors')

// Untuk upload file
const multer = require('multer')

// Untuk mengelola file
const fs = require('fs')

// Konfigurasi untuk upload foto makanan
const uploadFotoMakanan = multer({
  dest: 'uploads/foto-makanan/',
})

const app = express()
const port = 3100

app.use(cors({
  origin: '*'
}))

// GET
app.get('/makanan', (httpRequest, httpResponse) => {
  
  httpResponse.json([
  ])
})

// POST
app.post('/makanan', uploadFotoMakanan.single('foto'), (httpRequest, httpResponse) => {

  let lokasiFoto = null

  if(httpRequest.file) {

    // File hasil upload menggunakan Multer
    const fileHasilUpload = httpRequest.file

    // Jika ingin mengganti path atau file name
    // dari file yang baru diupload ini
    // dapat menggunakan package fs
    // Lokasi file yang baru diupload dapat diakses
    // melalui fileHasilUpload.path
    lokasiFoto = fileHasilUpload.path
    console.log("Berhasil diupload ke", lokasiFoto)

    // Memindahkan file dari lokasi lama ke lokasi baru

    // const lokasiFotoBaru = 'uploads/foto-makanan-baru' 
    // if(!fs.existsSync(lokasiFotoBaru)) {
    //   fs.mkdirSync(lokasiFotoBaru)
    // }

    // fs.rename(lokasiFoto, lokasiFoto.replace('foto-makanan', 'foto-makanan-baru'), () => {
    //   console.log("File dipindahkan")
    // })

  }

  httpResponse.json(
    {
      lokasi_foto: lokasiFoto,
    },
  )
})

app.listen(port, () => {
  console.log(`RESTful API berjalan di port ${port} `)
})

